const { test, expect } = require('@playwright/test');

test.describe('JSONPlaceholder API Tests', () => {
  let initialPostCount;
  let newPostId;
 

//Test to retrieve the total number of posts and save the number in a variable
  test('Get Total Number of Posts and Store in a Variable', async ({ request }) => {
    const response = await request.get('https://jsonplaceholder.typicode.com/posts');
    expect(response.status()).toBe(200);

    const posts = await response.json();
    initialPostCount = posts.length;
    console.log(`Initial number of posts: ${initialPostCount}`);
  })

  //Test to create a new post and save the ID for future use
  test('Create a New Post and Store its ID', async ({ request }) => {
    const newPost = {
      title: 'Test Post',
      body: 'This is a test post',
      userId: 1
    };
  
    const response = await request.post('https://jsonplaceholder.typicode.com/posts', { data: newPost });
    expect(response.status()).toBe(201);
  
    const createdPost = await response.json();
    newPostId = createdPost.id;
    console.log(`Created new post with ID: ${newPostId}`);
  });
  //Test to validate the endpoint can return only the newly created post
  test('Get Only the Created Post by ID', async ({ request }) => {
    const response = await request.get(`https://jsonplaceholder.typicode.com/posts/${newPostId}`);
  
    const post = await response.json();
    expect(post.id).toBe(newPostId);
    console.log('Retrieved post:', post);
    // Check that the response is 200
    if (response.status() === 200) {
        console.log('Successfully retrieved the created blog post');

    } else if (response.status() === 404) {
        console.log('Blog post not found');
    } else {
        console.log('Other error occurred');
    }

  });
  //Test to ensure new ID can be updated
  test('Replace Some Field in the Created Post with PATCH', async ({ request }) => {
    const updatedPostData = {
      title: 'Updated Test Post'
    };
  
    const response = await request.patch(`https://jsonplaceholder.typicode.com/posts/${newPostId}`, { data: updatedPostData });
    expect(response.status()).toBe(200);
  
    const updatedPost = await response.json();
    expect(updatedPost.title).toBe(updatedPostData.title);
    console.log('Updated post:', updatedPost);
  });
  //Test to ensure new ID can be deleted
  test('Delete the Created Post by ID', async ({ request }) => {
    const response = await request.delete(`https://jsonplaceholder.typicode.com/posts/${newPostId}`);
    expect(response.status()).toBe(200);
  
    // Attempt to retrieve the deleted post
    const getResponse = await request.get(`https://jsonplaceholder.typicode.com/posts/${newPostId}`);
    expect(getResponse.status()).toBe(404);
    console.log(`Post with ID ${newPostId} has been deleted.`);
  });
   //Test to validate the current number of posts
  test('Check the Number of Posts to Ensure Integrity', async ({ request }) => {
    const response = await request.get('https://jsonplaceholder.typicode.com/posts');
    expect(response.status()).toBe(200);
  
    const posts = await response.json();
    const currentPostCount = posts.length;
    expect(currentPostCount).toBe(initialPostCount);
    console.log(`Current number of posts: ${currentPostCount}`);
  });
});
