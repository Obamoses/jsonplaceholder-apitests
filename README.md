Playwright-API Tests-Project
Overview:

This is an API Automation Project using Playwright with Javascript to execute api test cases. 

Features

Allows transfer of data between test cases

All the Playwright related config is controlled by playwright config file

The tests can be found in api-tests.js file